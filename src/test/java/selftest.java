import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class selftest {

	WebDriver driver;
	
	
	@Test
	public void testName() throws Exception {
		
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://www.makemytrip.com");
		System.out.println(driver.getTitle());
			Thread.sleep(3000);
			
			
WebElement ad_frame= driver.findElement(By.xpath("//*[@id='webklipper-publisher-widget-container-notification-frame']"));			
			driver.switchTo().frame(ad_frame);
			
			driver.findElement(By.xpath("//*[@id='webklipper-publisher-widget-container-notification-close-div']")).click();
			
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div/main/div[9]/span")).click();
			Thread.sleep(5000);

			driver.findElement(By.xpath("//*[text()='DEPARTURE']")).click();
			
			
		String	expectmonth="November 2022";
		
		
		Thread.sleep(5000);
		
//		WebElement calender_frame = driver.findElement(By.xpath("//*[@id='destination_publishing_iframe_makemytrip_0']"));
//		
//		driver.switchTo().frame(calender_frame);
			
			String currentmonth = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/div[1]/div")).getText();
			
			if (currentmonth.equals(expectmonth)) {
				System.out.println("expected month already is selected");
			}
			else {
				for (int i = 1; i < 12; i++) {
					
				driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[1]/span[2]")).click();
				Thread.sleep(1000);
				String currentmonth1 = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/div[1]/div")).getText();
				if (currentmonth1.equals(expectmonth)) {
					System.out.println("expected month is selected");
					break;
				}
				
				
				}
				
			}
			
			String mydate="Tue Nov 29 2022";//Tue Nov 29 2022
			
			
			WebElement calender= driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/div[3]"));

			List<WebElement> arial=calender.findElements(By.className("DayPicker-Day"));
			
			for(WebElement webElement1 : arial) {
				
				String str=webElement1.getAttribute("aria-label");
								
				if (mydate.equals(str)) {
					webElement1.click();
				break;
				
				}
			
			}
			
			Thread.sleep(5000);
		driver.quit();
	}
	
	
	
	
	
	
	
}
