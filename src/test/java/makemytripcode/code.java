package makemytripcode;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com_makemytrip_base_package.baseclass;

public class code extends baseclass {


	public static void PageFactorymethod() {
		PageFactory.initElements(driver, code.class);

	}
	//click cab


	@FindBy(xpath = "//*/li[@data-cy='menu_Cabs']")
	public static WebElement  cabwebelement;

	public static void cab() {

		PageFactorymethod();
		cabwebelement.click();

	}

	// click from point

	@FindBy(xpath="//*/label[@for='fromCity']")
	public static WebElement  frompoint;
	public static void from_point() {
		PageFactorymethod();
		frompoint.click();

	}
	//from address

	@FindBy(xpath="//*/input[@placeholder='From']")
	public static WebElement startpoint;
	public static void from_loc_point(String string) {
		startpoint.sendKeys(string);
		confrim();
	}

	// confrim
	private static void confrim() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		startpoint.sendKeys(Keys.ARROW_DOWN);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		startpoint.sendKeys(Keys.ENTER);

	}
	// click to

	@FindBy(xpath="//label[@for='toCity']/span[text()='To']")
	public static WebElement Topoint;
	public static void To_point() {
		Topoint.click();

	}

	//from address

	@FindBy(xpath="//input[@placeholder='To']")
	public static WebElement To_loc_point;
	public static void To_loc_point(String string) {
		To_loc_point.sendKeys(string);
		confrim_to();
	}

	//confrim
	private static void confrim_to() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		To_loc_point.sendKeys(Keys.ARROW_DOWN);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		To_loc_point.sendKeys(Keys.ENTER);


	}

	//departure date


	public static void departuredate(String string) {

		String mydeparturedate=string;


		driver.findElement(By.xpath("//*[text()='DEPARTURE']")).click();


		String	expectmonth="November 2022";


		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}




		String currentmonth = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/div[1]/div")).getText();

		if (currentmonth.equals(expectmonth)) {
			System.out.println("expected month already is selected");
		}
		else {
			for (int i = 1; i < 12; i++) {

				driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[1]/span[2]")).click();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				String currentmonth1 = driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/div[1]/div")).getText();
				if (currentmonth1.equals(expectmonth)) {
					System.out.println("expected month is selected");
					break;
				}


			}

		}

		//		String mydate=//Tue Nov 29 2022;


		WebElement calender= driver.findElement(By.xpath("//*[@id='root']/div/div[2]/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/div[3]"));

		List<WebElement> arial=calender.findElements(By.className("DayPicker-Day"));

		for(WebElement webElement1 : arial) {

			String str=webElement1.getAttribute("aria-label");

			if (mydeparturedate.equals(str)) {
				webElement1.click();
				break;

			}

		}



		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		

	}

	public static void returndate(String string) {
		String myreturndate=string;
try {
	Thread.sleep(3000);
} catch (InterruptedException e2) {
	e2.printStackTrace();
}
		driver.findElement(By.xpath("//*/label/span[text()='RETURN']")).click();

		String	returnexpectmonth="December2022";

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		String currentmonthtext=driver.findElement(By.xpath("(//*/div[@class='DayPicker-Caption'])[1]")).getText();

		System.out.println(currentmonthtext);

		if (currentmonthtext.equals(returnexpectmonth)) {
			System.out.println("expected month already is selected");
		}
		else {
			for (int i = 1; i < 12; i++) {

				WebElement arrow=driver.findElement(By.xpath("(//*/div[@class='DayPicker-NavBar']/span)[2]"));
				arrow.click();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				String currentmonth1 = driver.findElement(By.xpath("//*/div[@class='DayPicker-Caption']")).getText();
				if (currentmonth1.equalsIgnoreCase(returnexpectmonth)) {
					System.out.println("expected month is selected");
					break;
				}


			}

		}

		WebElement calender= driver.findElement(By.xpath("(//*/div[@class='DayPicker-Body'])[1]"));

		List<WebElement> arial=calender.findElements(By.className("DayPicker-Day"));

		for(WebElement webElement1 : arial) {

			String str=webElement1.getAttribute("aria-label");

			if (myreturndate.equalsIgnoreCase(str)) {

				webElement1.click();

				break;

			}

		}

	}
	
	
	
	//pickup time

	public static void pickuptime(String string) {
		driver.findElement(By.xpath("//*/label[@for='pickupTime']/span[text()='PICKUP-TIME']")).click();
		////*/ul[@class='timeDropDown blackText']//frame
		
		String pickuptime=string;
//String time1=	driver.findElement(By.xpath("(//*/ul)[7]")).getText();
//System.out.println(time1);
		
//		JavascriptExecutor js =(JavascriptExecutor)driver;
//		js.executeAsyncScript("arguments[0].scrollIntoView();",driver.findElement(By.xpath("(//*/ul)[7]")).getText().equals(pickuptime));
		
		
		for (int i = 1; i < 288; i++) {
			String time=driver.findElement(By.xpath("(//*/ul)/li["+i+"]")).getText();
			
			if(pickuptime.equalsIgnoreCase(time)) {
				System.out.println("hi");
				driver.findElement(By.xpath("(//*/ul)/li["+i+"]")).click();
			break;
		}
		
		
	}


	
	
	}

	//droptime
	
	
	
	
	
	
	public static void droptime(String string) {
		
		String droptime1=string;
		
driver.findElement(By.xpath("//label[@for='dropTime']/span[text()='DROP-TIME']")).click();
for (int i = 1; i < 288; i++) {
	String time=driver.findElement(By.xpath("(//*/ul)/li["+i+"]")).getText();
	
	if(droptime1.equalsIgnoreCase(time)) {
		System.out.println("hi");
		driver.findElement(By.xpath("(//*/ul)/li["+i+"]")).click();
	break;
}



}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
