package makemytrip_stepdefinition;

import com_makemytrip_base_package.baseclass;
import io.cucumber.java.en.*;
import makemytripcode.code;

public class steps{

	//
	@Given("display {string}")
	public void display(String string) throws InterruptedException {
		baseclass.driver(string);
	}


	@Given("Select cab vechicle")
	public void select_cab_vechicle() {
		code.cab();

	}


	@Given("Select from to choose start travel point")
	public void select_from_to_choose_start_travel_point() {

		code.from_point();
	}


	@Then("given palce name {string}")
	public void given_palce_name(String string) {
		code.from_loc_point(string);

	}


	@Given("Click tab to switch To point")
	public void click_tab_to_switch_to_point() {

		code.To_point();
	}

	@Then("given palce names {string}")
	public void given_palce_names(String string) {
		code.To_loc_point(string);
	}


	@Given("click departure and select departure date {string}")
	public void click_departure_and_select_departure_date(String string)
	{
		code.departuredate(string);
	}


	@Given("click return and select return date {string}")
	public void click_return_and_select_return_date(String string)
	{
		code.returndate(string);
	}


	@Given("click the time to choose pick_up time {string}")
	public void click_the_time_to_choose_pick_up_time(String string)
	{
code.pickuptime(string);
	}


	@Given("click the time to choose drop time {string}")
	public void click_the_time_to_choose_drop_time(String string) 
	{
		code.droptime(string);
	}


























}
