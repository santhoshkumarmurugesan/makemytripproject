Feature: open makemytrip

  Scenario: open makemytrip website using correct url
    Given display "https://www.makemytrip.com"

  Scenario: Select vechicle
    Given Select cab vechicle

  Scenario: Select travel  from point
    Given Select from to choose start travel point
    Then given palce name "Tambaram, Chennai, Tamil Nadu, India"

  Scenario: Select travel  To point
    Given Click tab to switch To point
    Then given palce names "Tiruvannamalai, Tamil Nadu, India"

  Scenario: Select departure
    Given click departure and select departure date "Tue Nov 29 2022"

  Scenario: Select return
    Given click return and select return date "Tue dec 27 2022"
    
 Scenario: Select pick-up time
  Given click the time to choose pick_up time "12:50 AM"
  
  
  Scenario: Select drop time
  Given click the time to choose drop time "03:00 AM"